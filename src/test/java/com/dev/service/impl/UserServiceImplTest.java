package com.dev.service.impl;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import com.dev.exceptions.UserServiceException;
import com.dev.io.entity.AddressEntity;
import com.dev.io.entity.UserEntity;
import com.dev.io.repositories.UserRepository;
import com.dev.shared.AddressDto;
import com.dev.shared.AmazonSES;
import com.dev.shared.UserDto;
import com.dev.shared.Utils;



//class UserServiceImplTest {
//	
//	@InjectMocks
//	UserServiceImpl userService;
//	
//	@Mock
//	UserRepository userRepository;	
//
//	@Mock
//	Utils utils;
//	
//	@Mock
//	BCryptPasswordEncoder bCryptPasswordEncoder;
//	
//	@Mock
//	AmazonSES amazonSES;
//	
//	String userId = "ua78we9zx89web";
//	String encryptedPassword = "yg7wey98x8fssd0z970";
//	UserEntity userEntity;
//
//
//	@BeforeEach
//	void setUp() throws Exception {
//		MockitoAnnotations.initMocks(this);
//
//		userEntity = new UserEntity();
//		userEntity.setId(1L);
//		userEntity.setFirstName("Albert");
//		userEntity.setLastName("Mumo");
//		userEntity.setUserId(userId);
//		userEntity.setEncryptedPassword(encryptedPassword);
//		userEntity.setEmail("test@test.com");
//		userEntity.setEmailVerificationToken("hdasya89d9a87va9");
//		userEntity.setAddresses(getAddressEntity());
//	}
//
//	@Test
//	void testGetUser() {
//		
//		when(userRepository.findByEmail(anyString())).thenReturn(userEntity);
//		
//		UserDto userDto = userService.getUser("test@test.com");
//		
//		assertNotNull(userDto);
//		assertEquals("Albert", userDto.getFirstName());
//	}
//	
//	@Test
//	final void testGetUser_UsernameNotFoundException() {
//		when(userRepository.findByEmail(anyString())).thenReturn(null);
//	
//		assertThrows(UserServiceException.class, 
//				()-> {
//					userService.getUser("test@test.com");
//				}
//				);
//	}
//	
//	@Test
//	final void testCreateUser_CreateUserServiceException() {
//		when(userRepository.findByEmail(anyString())).thenReturn(userEntity);
//		
//		UserDto userDto = new UserDto();
//		userDto.setAddresses(getAddressesDto());
//		userDto.setFirstName("Albert");
//		userDto.setLastName("Mumo");
//		userDto.setPassword("w7A9S8DC");
//		userDto.setEmail("test@test.com");
//		
//		assertThrows(UserServiceException.class, 
//				()-> {
//					userService.createdUser(userDto);
//				}
//				); 
//	}
//	
//	@Test
//	final void testCreateUser() {
//		when(userRepository.findByEmail(anyString())).thenReturn(null);
//		when(utils.generateAddressId(anyInt())).thenReturn("as8c9as87da0sd89a");
//		when(utils.generateUserId(anyInt())).thenReturn(userId);
//		when(bCryptPasswordEncoder.encode(anyString())).thenReturn(encryptedPassword);
//		when(userRepository.save(any(UserEntity.class))).thenReturn(userEntity);
//		Mockito.doNothing().when(amazonSES).verifyEmail(any(UserDto.class));
//		
//		UserDto userDto = new UserDto();
//		userDto.setAddresses(getAddressesDto());
//		userDto.setFirstName("Albert");
//		userDto.setLastName("Mumo");
//		userDto.setPassword("w7A9S8DC");
//		userDto.setEmail("test@test.com");
//		
//		UserDto storedUserDetails = userService.createdUser(userDto);
//		
//		assertNotNull(storedUserDetails);
//		assertEquals(userEntity.getFirstName(), storedUserDetails.getFirstName());
//		assertEquals(userEntity.getLastName(), storedUserDetails.getLastName());
//		assertNotNull(storedUserDetails.getUserId());
//		assertEquals(storedUserDetails.getAddresses().size(), userEntity.getAddresses().size());
//		verify(utils, times(storedUserDetails.getAddresses().size())).generateAddressId(30);
//		verify(bCryptPasswordEncoder, times(1)).encode("w7A9S8DC");
//		verify(utils, times(1)).generateUserId(30);
//		verify(userRepository, times(1)).save(any(UserEntity.class));
//		
//	}
//	
//	private List<AddressDto> getAddressesDto(){
//		AddressDto addressDto = new AddressDto();
//		addressDto.setType("shipping");
//		addressDto.setCity("Eldoret");
//		addressDto.setCountry("Kenya");
//		addressDto.setPostalCode("cuas9d");
//		addressDto.setStreetName("Kwa Mahindi");
//		
//		AddressDto address2 = new AddressDto();
//		address2.setType("billing");
//		address2.setCity("Eldoret");
//		address2.setCountry("Kenya");
//		address2.setPostalCode("cuas9d");
//		address2.setStreetName("Kwa Mahindi");
//		
//		List<AddressDto> addresses = new ArrayList<>();
//		addresses.add(addressDto);
//		addresses.add(address2);
//		
//		return addresses;
//	}
//	
//	private List<AddressEntity> getAddressEntity(){
//		List<AddressDto> addresses = getAddressesDto();
//		
//		Type listType = new TypeToken<List<AddressEntity>>() {}.getType();
//		
//		return new ModelMapper().map(addresses, listType);
//	}
//
//}
