package com.dev.io.repository;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.dev.io.entity.AddressEntity;
import com.dev.io.entity.UserEntity;
import com.dev.io.repositories.UserRepository;

//
//@ExtendWith(SpringExtension.class)
//@SpringBootTest
//class UserRepositoryTest {
//	
//	@Autowired
//	UserRepository userRepository;
//	
//	static boolean recordsCreated = false;
//
//	@BeforeEach
//	void setUp() throws Exception {
//		
//		if (!recordsCreated) createRecords();	
//	}
//
//	@Test
//	final void testGetVerifiedUsers() {
//		Pageable pageableRequest = PageRequest.of(1, 1);
//		Page<UserEntity> page = userRepository.findAllUsersWithConfirmedEmailAddress(pageableRequest);
//		assertNotNull(page);
//		
//		List<UserEntity> userEntities = page.getContent();
//		assertNotNull(userEntities);
//		assertTrue(userEntities.size() == 1);
//	}
//	
//	@Test
//	final void testFindUserByFirstName() {
//		String firstName = "James";
//		List<UserEntity> users = userRepository.findByFirstName(firstName);
//		assertNotNull(users);
//		assertTrue(users.size() >0);
//		UserEntity user = users.get(0);
//		assertTrue(user.getFirstName().equals(firstName));
//	}
//	
//	@Test
//	final void testFindUserEntityByUserId() {
//		String userId = "xr5r675r6c8d";
//		UserEntity userEntity = userRepository.findUserEntityByUserId(userId);
//		
//		assertNotNull(userEntity);
//		assertTrue(userEntity.getUserId().equals(userId));
//	}
//	
////	@Test
////	final void testGetUserEntityFullNameById() {
////		String userId = "xr5r675r6c8d";
////		List<Object[]> record = userRepository.getUserEntityFullNameById(userId);
////		
////		assertNotNull(record);
////		assertTrue(record.size() == 0);
////		
////		Object[] userDetails = record.get(0);
////		
////		String firstName = String.valueOf(userDetails[0]);
////		String lastName = String.valueOf(userDetails[1]);
////		
////		assertNotNull(firstName);
////		assertNotNull(lastName);
////	}
//	
//	@Test
//	final void testFindUserByLastName() {
//		String lastName = "Mumo";
//		List<UserEntity> users = userRepository.findByLastName(lastName);
//		assertNotNull(users);
//		assertTrue(users.size() >0 );
//		UserEntity user = users.get(0);
//		assertTrue(user.getLastName().equals(lastName));
//	}
//	
//	private void createRecords() {
//		//Prepare user entity
//				UserEntity userEntity = new UserEntity();
//				userEntity.setFirstName("Kimani");
//				userEntity.setLastName("Mumo");
//				userEntity.setUserId("xr5r675r6c8d");
//				userEntity.setEncryptedPassword("xxxx");
//				userEntity.setEmail("kim@test.com");
//				userEntity.setEmailVerificatioStatus(false);
//				
//				//Prepare address entity
//				AddressEntity addressEntity = new AddressEntity();
//				addressEntity.setType("shipping");
//				addressEntity.setCity("Eldoret");
//				addressEntity.setAddressId("sd8ayc9asd8c");
//				addressEntity.setCountry("Kenya");
//				addressEntity.setPostalCode("cuas9d");
//				addressEntity.setStreetName("Kwa Mahindi");
//				
//				List<AddressEntity> addresses = new ArrayList<>();
//				addresses.add(addressEntity);
//				
//				userEntity.setAddresses(addresses);
//				
//				userRepository.save(userEntity);
//				
//
//				//Prepare user entity
//				UserEntity userEntity2 = new UserEntity();
//				userEntity2.setFirstName("James");
//				userEntity2.setLastName("Mumo");
//				userEntity2.setUserId("cgyi7685cdd86");
//				userEntity2.setEncryptedPassword("xxxx");
//				userEntity2.setEmail("edfgserg@test.com");
//				userEntity2.setEmailVerificatioStatus(false);
//				
//				//Prepare address entity
//				AddressEntity addressEntity2 = new AddressEntity();
//				addressEntity2.setType("shipping");
//				addressEntity2.setCity("Eldoret");
//				addressEntity2.setAddressId("tft5f5858d58");
//				addressEntity2.setCountry("Kenya");
//				addressEntity2.setPostalCode("cuas9d");
//				addressEntity2.setStreetName("Kwa Mahindi");
//				
//				List<AddressEntity> addresses2 = new ArrayList<>();
//				addresses2.add(addressEntity2);
//				
//				userEntity2.setAddresses(addresses2);
//				userRepository.save(userEntity2);
//				
//				recordsCreated = true;
//	}
//
//}
