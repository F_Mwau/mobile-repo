package com.dev;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.service.VendorExtension;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;


@Configuration
@EnableSwagger2
public class SwaggerConfig {
	
	Contact contact = new Contact(
			"Fredrick Mutua",
			"fredrickmutua12@gmail,com",
			"mutuahfredrick@gmail.com");
	
	List<VendorExtension> vendorExtension = new ArrayList<>();
	
	ApiInfo apiInfo = new ApiInfo(
			"Mobile App RESTFUL Web Service Endpoints",
			"This pages the documents of Mobile App RESTFUL Web Endpoints",
			"1.0",
			"No current app blog/email available",
			contact,
			"Apache 2.0",
			"http://www.apache.org/licences/LICENCE-2.0",
			vendorExtension);
	
	@Bean
	public Docket apiDocket() {
		
		Docket docket = new Docket(DocumentationType.SWAGGER_2)
				.protocols(new HashSet<>(Arrays.asList("HTTP", "HTTPS")))
				.apiInfo(apiInfo)
				.select()
				.apis(RequestHandlerSelectors.basePackage("com.dev"))
				.paths(PathSelectors.any())
				.build();	
		return docket;
	}
}
