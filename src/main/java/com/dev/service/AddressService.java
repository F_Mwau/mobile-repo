package com.dev.service;

import java.util.List;

import com.dev.shared.AddressDto;

public interface AddressService {
	List<AddressDto> getAddresses(String userId);
	AddressDto getAddress(String addressId);
}