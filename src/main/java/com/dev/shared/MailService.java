package com.dev.shared;


import java.util.Date;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;

import javax.mail.Session;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import com.sun.mail.smtp.SMTPTransport;

@Component
public class MailService {
	
	@Autowired
	private Environment env;
	final String SUBJECT = "One last step to complete your registration on mobile_app";
	final String HTMLBODY = "Please verify your email address"
			+ "Thank you for registering with our mobile app. To complete registration process and be able to log in"
			+ "click on the following link: "
			+ "Final step to complete your registration"
			+ "Thank you! And we are waiting for you inside!";
	
	final String TEXTBODY = "Please verify your email address. "
			+ "Thank you for registering with our mobile app. To complete registration process and be able to log in"
			+ "open the following link into your URL Browser window \n	"
			+ "\n http://localhost:8080/verification-service/email-verification.html?token=tokenValue \n"
			+ "\n Thank you! And we are waiting for you inside!"; 
	
	final String PASSWORD_RESET_HEADBODY = "A request to reset your password \n"
			+ "Hi";
	
	final String PASSWORD_RESET_TEXTBODY = 
			 "Someone has requested to reset your password with our project. If it wasn't you don't click on this link below!! \n"
			+ "otherwise please click on the link below in your browser window to set a new password \n"
			+ "Thank you! \n";
	

	public String sendmail(String token,String email) throws AddressException, MessagingException  {
		
		
		String textBodyWithToken = TEXTBODY + "http://localhost:8080/verification-service/email-verification.html?token="
				+ token;
		Properties props = System.getProperties();
//        props.put("mail.smtps.host","mail.skyplus.co.ke");		
		 props.getProperty("mail.smtps.host");
		 props.getProperty("mail.smtps.auth");
		String senderEmail = env.getProperty("mail.semail");
        String smtpHost = env.getProperty("mail.host");       
        String smtpPassword = env.getProperty("mail.spwd");
        System.out.println("senderEmail: " +senderEmail);
        System.out.println("smtpHost: " + env.getProperty("mail.smtps.host") );
        System.out.println("smtpPassword: " +   smtpPassword);
        Session session = Session.getInstance(props, null);
        Message msg = new MimeMessage(session);
        msg.setFrom(new InternetAddress(senderEmail));;
        msg.setRecipients(Message.RecipientType.TO,
        InternetAddress.parse(email, false));
        msg.setSubject(SUBJECT);
        msg.setText(textBodyWithToken);
        msg.setHeader("X-Mailer", "Tov Are's program");
        msg.setSentDate(new Date());
        SMTPTransport t =
            (SMTPTransport)session.getTransport("smtps");
        t.connect(smtpHost, senderEmail,smtpPassword );
        t.sendMessage(msg, msg.getAllRecipients());
        t.getLastReturnCode();
        System.out.println("Response: " + t.getLastServerResponse());
        System.out.println("Response2: " +  t.getReportSuccess());
        System.out.println("Response3: " +   t.getLastReturnCode());
        t.close();
        
        
        return "";
		}
	
	public String passwordReset() throws AddressException, MessagingException  {
//		 boolean returnValue = false;
		
		 String textBodyWithToken = PASSWORD_RESET_HEADBODY  + PASSWORD_RESET_TEXTBODY + "http://localhost:8080/verification-service/email-verification.html?token="
				;
//		 	textBodyWithToken = textBodyWithToken.replace("$firstName", name);
		Properties props = System.getProperties();
		props.getProperty("mail.smtps.host");
		props.getProperty("mail.smtps.auth");
		String senderEmail = env.getProperty("mail.semail");
        String smtpHost = env.getProperty("mail.host");       
        String smtpPassword = env.getProperty("mail.spwd");
        Session session = Session.getInstance(props, null);
        Message msg = new MimeMessage(session);
        msg.setFrom(new InternetAddress("info@skyplus.co.ke"));;
        msg.setRecipients(Message.RecipientType.TO,
        InternetAddress.parse("fredrickmutua12@gmail.com", false));
        msg.setSubject(SUBJECT);
        msg.setText(textBodyWithToken);
        msg.setHeader("X-Mailer", "Tov Are's program");
        msg.setSentDate(new Date());
        SMTPTransport t =
            (SMTPTransport)session.getTransport("smtps");
        t.connect(smtpHost, senderEmail, smtpPassword);
        t.sendMessage(msg, msg.getAllRecipients());
        System.out.println("Response: " + t.getLastServerResponse());
        System.out.println("Response2: " +  t.getReportSuccess());
    
//        if (!t.getLastServerResponse().isEmpty()) {
//        	returnValue = true;
//        }
        t.close();
        
      
        
         return "";
		}

	
}
