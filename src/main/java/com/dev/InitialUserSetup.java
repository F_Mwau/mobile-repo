package com.dev;

import java.util.Arrays;
import java.util.Collection;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.dev.io.entity.AuthorityEntity;
import com.dev.io.entity.RoleEntity;
import com.dev.io.repositories.AuthorityRepository;
import com.dev.io.repositories.RoleRepository;
import com.dev.io.repositories.UserRepository;
import com.dev.shared.Roles;
import com.dev.shared.Utils;

@Component
public class InitialUserSetup {
	
	@Autowired
	AuthorityRepository authorityRepository;
	
	@Autowired
	RoleRepository roleRepository;
	
	@Autowired
	UserRepository userRepository;
	
	@Autowired
	Utils utils;
	
	@Autowired
	BCryptPasswordEncoder bCryptPasswordEncoder;
	
	@EventListener
	@Transactional
	public void onApplicationEvent(ApplicationReadyEvent event) {
		System.out.println("From Application ready event ....");
		
		AuthorityEntity readAuthority = createAuthority("READ_AUTHORITY");
		AuthorityEntity writeAuthority = createAuthority("WRITE_AUTHORITY");
		AuthorityEntity deleteAuthority = createAuthority("DELETE_AUTHORITY");
		
		createRole(Roles.ROLE_USER.name(), Arrays.asList(readAuthority,writeAuthority));
		RoleEntity roleAdmin = createRole(Roles.ROLE_ADMIN.name(), Arrays.asList(readAuthority,writeAuthority,deleteAuthority));
		
		if(roleAdmin == null) return;
//		
//		default admin
//		
//		UserEntity adminUser = new UserEntity();
//		adminUser.setFirstName("Fred");
//		adminUser.setLastName("Mwau");
//		adminUser.setEmail("admin@test.com");
//		adminUser.setEmailVerificatioStatus(true);
//		adminUser.setUserId(utils.generateUserId(30));
//		adminUser.setEncryptedPassword(bCryptPasswordEncoder.encode("12345678"));
//		adminUser.setRoles(Arrays.asList(roleAdmin));
//		
//		userRepository.save(adminUser);
	}
	
	@Transactional
	private AuthorityEntity createAuthority(String name) {
		
		AuthorityEntity authority = authorityRepository.findByName(name);
		if (authority == null) {
			authority = new AuthorityEntity(name);
			authorityRepository.save(authority);
		}
		return authority;
	}
	
	@Transactional
	private RoleEntity createRole (
			String name, Collection<AuthorityEntity> authorities) {
		
		RoleEntity role = roleRepository.findByName(name);
		if (role == null) {
			role = new RoleEntity(name);
			role.setAuthorities(authorities);
			roleRepository.save(role);
		}
		return role;
	}
}
