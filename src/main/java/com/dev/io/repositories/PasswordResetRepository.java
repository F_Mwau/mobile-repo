package com.dev.io.repositories;

import org.springframework.data.repository.CrudRepository;

import com.dev.io.entity.PasswordResetTokenEntity;

public interface PasswordResetRepository extends CrudRepository<PasswordResetTokenEntity, Long> {
	PasswordResetTokenEntity  findByToken(String token);
}
