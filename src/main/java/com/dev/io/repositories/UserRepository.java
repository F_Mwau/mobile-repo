package com.dev.io.repositories;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.dev.io.entity.UserEntity;

@Repository
public interface UserRepository extends PagingAndSortingRepository<UserEntity, Long> {
	UserEntity findByEmail(String email);
	UserEntity findByUserId(String userId);
	UserEntity findUserByEmailVerificationToken(String token);
	
	@Query(value = "SELECT * FROM `users` WHERE `email_verificatio_status` = 'true' ", 
			countQuery = "SELECT count(*) FROM `users` WHERE `email_verificatio_status` = 'true' ",
			nativeQuery = true)
	Page<UserEntity> findAllUsersWithConfirmedEmailAddress(Pageable pageableRequest);
	
	@Query(value = "SELECT * FROM `users` WHERE `first_name` = ?1 ",
			nativeQuery = true)
	List<UserEntity> findByFirstName(String firstName);
		
	@Query(value = "SELECT * FROM `users` WHERE `last_name` = :lastName",
			nativeQuery = true) 		
	List<UserEntity> findByLastName(@Param ("lastName")String lastName); 
	
	@Query("select user from UserEntity user where user.userId = :userId")
	UserEntity findUserEntityByUserId(@Param("userId")String userId);
	
//	@Query("select user.firstName, user.lastName from UserEntity user where user.userId = :userid")
//	List<Object[]> getUserEntityFullNameById(@Param("userId") String userId);
}
