package com.dev.exceptions;

public class UserServiceException extends RuntimeException{

	
	private static final long serialVersionUID = 1548068978265834075L;
	
	public UserServiceException(String message) {
		super(message);
	}
}
