package com.dev.ui.controller;


import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.dev.exceptions.UserServiceException;
import com.dev.service.AddressService;
import com.dev.service.UserService;
import com.dev.shared.AddressDto;
import com.dev.shared.MailService;
import com.dev.shared.Roles;
import com.dev.shared.UserDto;
import com.dev.ui.model.request.PasswordResetModel;
import com.dev.ui.model.request.PasswordResetRequestModel;
import com.dev.ui.model.request.UserDetailsRequestModel;
import com.dev.ui.model.response.AddressRest;
import com.dev.ui.model.response.ErrorMessages;
import com.dev.ui.model.response.OperationStatusModel;
import com.dev.ui.model.response.RequestOperationStatus;
import com.dev.ui.model.response.UserRest;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("users")
public class UserContoller {

	@Autowired
	UserService userService;

	@Autowired
	AddressService addressesService;

	@Autowired
	AddressService addressService;
	
	@Autowired
	MailService mailer;
	
	/* http://localhost:880/mobile_app/users/userID */
	@ApiImplicitParams ({
		@ApiImplicitParam(name = "authorization", value = "${userController.authorizationHeader,description}", paramType = "header")
	})
	@ApiOperation(value = "The Get User Details Web Service Endpoint",
	notes = "${userController.GetUser.ApiOperation.Notes}")
	@GetMapping(path = "/{id}", produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
	public UserRest getUser(@PathVariable String id) {

		UserRest returnValue = new UserRest();
		UserDto userDto = userService.getUserByUserId(id);
		BeanUtils.copyProperties(userDto, returnValue);
		return returnValue;
	}

	/* http://localhost:880/mobile_app/users */
	@ApiOperation(value = "The Create User Web Service Endpoint",
			notes = "${userController.CreateUser.ApiOperation.Notes}")
	@PostMapping(produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE }, consumes = {
			MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
	public UserRest createUser(@RequestBody UserDetailsRequestModel userDetails) throws Exception {
		UserRest returnValue = new UserRest();

//		UserDto userDto = new UserDto();
//		BeanUtils.copyProperties(userDetails, userDto);

		ModelMapper modelMapper = new ModelMapper();
		UserDto userDto = modelMapper.map(userDetails, UserDto.class);
		userDto.setRole(new HashSet<>(Arrays.asList(Roles.ROLE_USER.name()	)));

		if (userDetails.getFirstName().isEmpty())
			throw new UserServiceException(ErrorMessages.MISSING_REQUIRED_FIELD.getErrorMessage());

		UserDto createUser = userService.createdUser(userDto);
		returnValue = modelMapper.map(createUser, UserRest.class);
		return returnValue;
	}

	/* http://localhost:880/mobile_app/users/userID */
	@ApiImplicitParams ({
		@ApiImplicitParam(name = "authorization", value = "${userController.authorizationHeader,description}", paramType = "header")
	})
	@ApiOperation(value = "The Update User Details Web Service Endpoint",
	notes = "${userController.UpdateUser.ApiOperation.Notes}")
	@PutMapping(path = "/{id}", produces = { MediaType.APPLICATION_JSON_VALUE,
			MediaType.APPLICATION_XML_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE,
					MediaType.APPLICATION_XML_VALUE })
	public UserRest updateUser(@PathVariable String id, @RequestBody UserDetailsRequestModel userDetails) {
		UserRest returnValue = new UserRest();

		UserDto userDto = new UserDto();
		BeanUtils.copyProperties(userDetails, userDto);

		UserDto updatedUser = userService.updateUser(id, userDto);
		BeanUtils.copyProperties(updatedUser, returnValue);

		return returnValue;
	}

	/* http://localhost:880/mobile_app/users/userID */
	@ApiImplicitParams ({
		@ApiImplicitParam(name = "authorization", value = "${userController.authorizationHeader,description}", paramType = "header")
	})
	@ApiOperation(value = "The Delete User Details Web Service Endpoint",
	notes = "${userController.DeleteUser.ApiOperation.Notes}")
	//@Secured("ROLE_ADMIN")
	@PreAuthorize ("hasRole('ROLE_ADMIN') or #id == principal.userId")
	@DeleteMapping(path = "/{id}", produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
	public OperationStatusModel deleteUser(@PathVariable String id) {

		OperationStatusModel returnValue = new OperationStatusModel();
		returnValue.setOperationName(RequestOperationName.DELETE.name());

		userService.deleteUser(id);

		returnValue.setOperationResult(RequestOperationStatus.SUCCESS.name());

		return returnValue;
	}

	/* http://localhost:880/mobile_app/users?page = pageNo & limit = limitNo */
	@ApiImplicitParams ({
		@ApiImplicitParam(name = "authorization", value = "${userController.authorizationHeader,description}", paramType = "header")
	})
	@ApiOperation(value = "The Get Users List Details Web Service Endpoint",
	notes = "${userController.GetUsers.ApiOperation.Notes}")
	@PostAuthorize("hasRole('ROLE_ADMIN') or returnObject.userId == principal.userId")
	@GetMapping(produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
	public List<UserRest> getUsers(@RequestParam(value = "page", defaultValue = "0") int page,
			@RequestParam(value = "limit", defaultValue = "25") int limit) {
		List<UserRest> returnValue = new ArrayList<>();

		List<UserDto> users = userService.getUsers(page, limit);

		for (UserDto userDto : users) {
			UserRest userModel = new UserRest();
			BeanUtils.copyProperties(userDto, userModel);
			returnValue.add(userModel);
		}

		return returnValue;
	}

	// http://localhost:8080/mobile_app/users/**userId**/addresses/
	@ApiImplicitParams ({
		@ApiImplicitParam(name = "authorization", value = "${userController.authorizationHeader,description}", paramType = "header")
	})
	@ApiOperation(value = "The Get Users Addresses List Details Web Service Endpoint",
	notes = "${userController.GetAddresses.ApiOperation.Notes}")
	@GetMapping(path = "/{id}/addresses", produces = { MediaType.APPLICATION_JSON_VALUE,
			MediaType.APPLICATION_XML_VALUE })
	public List<AddressRest> getUserAddresses(@PathVariable String id) {

		List<AddressRest> returnValue = new ArrayList<>();

		List<AddressDto> addressDto = addressesService.getAddresses(id);

		if (addressDto != null && !addressDto.isEmpty()) {
			Type listType = new TypeToken<List<AddressRest>>() {
			}.getType();
			returnValue = new ModelMapper().map(addressDto, listType);
		}

		return returnValue;
	}

	// http://localhost:8080/mobile_app/users/**userId**/addresses/addressID
	@ApiImplicitParams ({
		@ApiImplicitParam(name = "authorization", value = "${userController.authorizationHeader,description}", paramType = "header")
	})
	@ApiOperation(value = "The Get Address Details Web Service Endpoint",
	notes = "${userController.GetAddress.ApiOperation.Notes}")
	@GetMapping(path = "/{id}/addresses/{addressId}", produces = { MediaType.APPLICATION_JSON_VALUE,
			MediaType.APPLICATION_XML_VALUE })
	public AddressRest getUserAddress(@PathVariable String userId, @PathVariable String addressId) {
		AddressDto addressDto = addressService.getAddress(addressId);

		ModelMapper modelMapper = new ModelMapper();

		return modelMapper.map(addressDto, AddressRest.class);
	}

	// http://localhost:8080/mobile_app/users/email-verification
	@ApiOperation(value = "The Verify Email Address Web Service Endpoint",
			notes = "${userController.VerifyEmail.ApiOperation.Notes}")
	@GetMapping(path = "/email-verification", produces = { MediaType.APPLICATION_JSON_VALUE,
			MediaType.APPLICATION_XML_VALUE })
	public OperationStatusModel verifyEmailToken(@RequestParam(value = "token") String token) {
		OperationStatusModel returnValue = new OperationStatusModel();
		returnValue.setOperationName(RequestOperationName.VERIFY_EMAIL.name());

		boolean isVerified = userService.verifyEmailToken(token);

		if (isVerified) {
			returnValue.setOperationResult(RequestOperationStatus.SUCCESS.name());
		} else {
			returnValue.setOperationResult(RequestOperationStatus.ERROR.name());
		}

		return returnValue;
	}

	
	/* http://localhost:8080/mobile_app/users/password-reset-request */
	@ApiOperation(value = "The Request Password Reset Web Service Endpoint",
			notes = "${userController.RequestReset.ApiOperation.Notes}")
	@PostMapping(path = "/password-reset-request", produces = { MediaType.APPLICATION_JSON_VALUE,
			MediaType.APPLICATION_XML_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE,
					MediaType.APPLICATION_XML_VALUE })
	public OperationStatusModel requestReset(@RequestBody PasswordResetRequestModel passwordResetRequestModel) {
		OperationStatusModel returnValue = new OperationStatusModel();
		
		boolean operationResult = userService.requestPasswordReset(passwordResetRequestModel.getEmail());
		System.out.println(operationResult);
		returnValue.setOperationName(RequestOperationName.REQUEST_PASSWORD_RESET.name());
		returnValue.setOperationResult(RequestOperationStatus.SUCCESS.name());
		
		if (!operationResult) {
			returnValue.setOperationResult(RequestOperationStatus.ERROR.name());
				}
		
		return returnValue;
	}
	
	/* http://localhost:8080/mobile_app/users/password-reset */

	@ApiOperation(value = "The Reset Password Web Service Endpoint",
			notes = "${userController.Reset.ApiOperation.Notes}")
	@PostMapping(path = "/password-reset",
			consumes = { MediaType.APPLICATION_JSON_VALUE,MediaType.APPLICATION_XML_VALUE })					
	public OperationStatusModel resetPassword(@RequestBody PasswordResetModel passwordResetModel) {
		OperationStatusModel returnValue = new OperationStatusModel();
		
		boolean operationResult = userService.resetPassword(
				passwordResetModel.getToken(),
				passwordResetModel.getPassword());
		returnValue.setOperationName(RequestOperationName.PASSWORD_RESET.name());
		returnValue.setOperationResult(RequestOperationStatus.ERROR.name());
		
		if (operationResult) {
			returnValue.setOperationResult(RequestOperationStatus.SUCCESS.name());
		}
		return returnValue;
	}

}
