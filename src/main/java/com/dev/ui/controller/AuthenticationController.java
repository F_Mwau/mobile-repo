package com.dev.ui.controller;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.dev.ui.model.request.LogInRequestModel;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.ResponseHeader;

@RestController
public class AuthenticationController {
	
	@ApiOperation("UserLogin")
	@ApiResponses(value = {
			@ApiResponse(code = 200,
					message = "REsponse Headers",
					responseHeaders = {
							@ResponseHeader(name = "authorization",
									description = "Bearer <JWT Web Token here>",
									response = String.class),
							@ResponseHeader(name = "userId",
							description = "<Public userID value here>",
							response = String.class)
					})
	})

	@PostMapping("/users/login")
	public void fakeLogin(@RequestBody LogInRequestModel logInRequestModel) {
		throw new IllegalStateException("This method shouldn't be caled since its implemented bby Spring Framework");
	}
}
	 